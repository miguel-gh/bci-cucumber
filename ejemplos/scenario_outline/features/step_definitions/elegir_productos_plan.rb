Given(/^Ingreso al sitio de Viajes$/) do
  visit $url_app
end

When(/^Ingreso mi edad$/) do
  fill_in "edad", :with => '33'
end

When(/^Ingreso mi renta liquida menor a \$(\d+)\.(\d+)$/)  do |arg0, arg1|
  sleep 1
  find("#renta").click
  find("#renta > div > div > input").send_keys("$0 a $399.999")
  find("#ui-select-choices-row-0-0 > div > div").click
end

When(/^Ingreso mi renta liquida mayor a \$(\d+)\.(\d+)$/) do |arg0, arg1|
  sleep 1
  find("#renta").click
  find("#renta > div > div > input").send_keys("$2.500.000 a $2.999.999")
  find("#ui-select-choices-row-0-0 > div > div").click
end

When(/^Presiono el boton Quiero conocer mi plan$/) do
  click_button "Quiero conocer mi plan"
end

Then(/^No ingresa a la seleccion de plan$/) do
  expect(page).to have_content("Lamentamos no poder atender su solicitud")
  save_screenshot
end

Then(/^Ingreso a la Seleccion de Plan$/) do
  sleep 1
  expect(page).to have_content("Escoja el plan que necesite")
  save_screenshot
end

Given(/^Selecciono el Plan Bci AAdvantage$/) do
  find("body > div.ng-scope > ui-view > div > section > div > div.row > div:nth-child(2) > div > div.acenter > a").click
end

When(/^Presiono el boton Quiero este Plan$/) do
end

When(/^Presiono el boton Comience$/) do
  sleep 1
  find("body > div.ng-scope > ui-view > div.main-bg.ng-scope > div.main-flux > div > div.buttons-at-bottom > button").click
end

When(/^Ingreso los datos personales "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)"$/) do |nombre, a_paterno, a_materno, rut, nro_serie, telefono, email|
  fill_in "nombre", :with => nombre
  fill_in "apellidoPaterno", :with => a_paterno
  fill_in "apellidoMaterno", :with => a_materno
  fill_in "rut", :with => rut
  fill_in "numeroSerieDocumento", :with => nro_serie
  fill_in "telefono", :with => telefono
  fill_in "correoElectronico", :with => email
end

When(/^Presiono Continue$/) do
  find("body > div.ng-scope > ui-view > div.main-bg.ng-scope > div.main-flux > div > form > div.buttons-at-bottom > a.btn.pilot-btn.pull-right").click
end

Then(/^Despliega la pantalla de deposito$/) do
  sleep 3
  expect(page).to have_content("Lo sentimos")
  save_screenshot
end
