Feature: Landing Page
Ofrecer planes a personas que quieran formar parte del banco.

Background: Ingresar al landing Page
  Given Quiero formar parte del banco
  When Ingreso a la aplicacion de Planes
  Then Se despliega el landing page de Planes

Scenario: Presentar un Plan Universitario
  Given Existe un cliente universitario
  When Ingresa su edad menor o igual a 28 años
  And Ingresa su renta menor a 400 000 mil pesos
  And No selecciona la opción de independiente
  And Presiona el botón Quiero Conocer Mi Plan
  Then Se despliega el formulario del Plan Universario
