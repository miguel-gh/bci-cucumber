Given(/^Quiero formar parte del banco$/) do
end

When(/^Ingreso a la aplicacion de Planes$/) do
  visit 'https://www.bci.cl/webPublico/planes.html#/contratoplanes/landing'
end

Then(/^Se despliega el landing page de Planes$/) do
end

Given(/^Existe un cliente universitario$/) do
end

When(/^Ingresa su edad menor o igual a (\d+) años$/) do |arg1|
  fill_in 'edad', with: '18'
end

When(/^Ingresa su renta menor a (\d+) (\d+) mil pesos$/) do |arg1, arg2|
  find('#renta > a').click
  find('#renta > div > div > input').send_keys('$0 a $399.999')
  find('#ui-select-choices-row-0-0 > div > div').click
  sleep 3
end

When(/^No selecciona la opción de independiente$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

When(/^Presiona el botón Quiero Conocer Mi Plan$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

Then(/^Se despliega el formulario del Plan Universario$/) do
  pending # Write code here that turns the phrase above into concrete actions
end
