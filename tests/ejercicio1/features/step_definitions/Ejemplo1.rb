Given(/^Soy un usuario interesado en abrir un nueva cuenta corriente$/) do
end

When(/^Ingreso al sitio de Planes BCI$/) do
  visit $url
  save_screenshot
end

Then(/^Se despliega el sitio de planes$/) do
end

Given(/^Ingreso la edad "([^"]*)"$/) do |edad|
  fill_in 'edad', with: edad
end

Given(/^Ingreso la renta "([^"]*)"$/) do |renta|
  find('#renta > a').click
  find('#renta > div > div > input').send_keys( renta )
  find('#ui-select-choices-row-0-0 > div > div').click
end

When(/^Presiono el boton$/) do
  # binding.pry
  # sleep 1
  # click_button('Quiero conocer mi plan')
  save_screenshot
end

Then(/^Se despliega el Plan Universitario$/) do
  # sleep 5
  # expect(page).to have_content("Plan Bci Universitario")
end
