Feature: Desplegar landing page de Viajes Planes
Desplegar a nuevos clientes un landing para abrir cuentas corrientes

Background: Ingresar al sitio de viajes Planes
    Given Soy un usuario interesado en abrir un nueva cuenta corriente
    When Ingreso al sitio de Planes BCI
    Then Se despliega el sitio de planes

@smoketest
Scenario: Desplegar un Plan Bci Universitario
    Given Ingreso la edad "18"
    And Ingreso la renta "$0 a $399.999"
    When Presiono el boton
    Then Se despliega el Plan Universitario

@outline
Scenario Outline: Despliegue de Planes
    Given Ingreso la edad "<edad>"
    And Ingreso la renta "<renta>"
    When Presiono el boton
    Then Se despliega el Plan Universitario

    Examples:
			| edad 		| renta |
			| 18	| $0 a $399.999 |
      | 20	| $0 a $399.999 |
      | 30	| $0 a $399.999 |
      | 40	| $0 a $399.999 |
