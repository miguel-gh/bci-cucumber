# -*- coding: UTF-8 -*-
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'pry'
require 'capybara-screenshot/cucumber'

caps = {
  :platform => "Mac OS X 10.9",
  :browserName => "Chrome",
  :version => "31",
}

Selenium::WebDriver.for(:remote,
    :url => "https://pazua:ba358838-c561-4d03-8101-ffc750a5929d@ondemand.saucelabs.com:443/wd/hub",
    :desired_capabilities => caps)

# browser setup
# browser = ENV['BROWSER']
# if browser == 'chrome'
#   Capybara.register_driver :selenium do |app|
#     # Capybara::Selenium::Driver.new(app, :browser => :chrome, :switches => %w[-incognito --disable-popup-blocking
#     # #--ignore-certificate-errors --disable-translate])
#     # Capybara::Selenium::Driver.new(app, :browser => :chrome)
#     Capybara::Selenium::Driver.new(remote_caps)
#   end
# end
# Capybara.default_driver = :selenium

# url app setup
$url = ENV['URL']

# evidencia - Capybara Screenshot
Capybara.save_and_open_page_path = './images'
Capybara::Screenshot.autosave_on_failure = true
