Feature: Ingresar Renta
	Elegir Productos de Plan

	Scenario: Ingreso de Renta Mayor a 400.000 pesos
		Given Ingreso al sitio de Viajes
		When Ingreso mi edad
		And Ingreso mi renta liquida mayor a $400.000
		And Presiono el boton Quiero conocer mi plan
		Then Ingreso a la Seleccion de Plan

  Scenario: Ingreso de Renta Menor a 400.000 pesos
    Given Ingreso al sitio de Viajes
    When Ingreso mi edad
    And Ingreso mi renta liquida menor a $400.000
    And Presiono el boton Quiero conocer mi plan
    Then No ingresa a la seleccion de plan
