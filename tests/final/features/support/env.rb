# -*- coding: UTF-8 -*-
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'capybara-screenshot/cucumber'
require 'pry'

# Capybara Settings
if ENV['BROWSER'] == 'chrome'
  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome, :switches => %w[-incognito --disable-popup-blocking
    #--ignore-certificate-errors --disable-translate])
  end
end

Capybara.default_driver = :selenium

# Capybara Screenshot
Capybara.save_and_open_page_path = './images'
Capybara::Screenshot.autosave_on_failure = true

# Global Variables
$url = ENV['URL']
$app = ENV['APP']
$url_app = $url + $app

# $urls = {
#   login: "#{$url_app}/login",
#   proyectos: "#{$url_app}/#/menu/proyectos",
#   features: "#{$url_app}/#/features"
# }
