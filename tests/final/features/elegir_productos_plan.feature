@CPM00050
Feature: Elegir Productos de Plan
	Elegir Productos de Plan

	Background: Ingreso de Renta Mayor a 400.000 pesos
		Given Ingreso al sitio de Viajes
		When Ingreso mi edad
		And Ingreso mi renta liquida mayor a $400.000
		And Presiono el boton Quiero conocer mi plan
		Then Ingreso a la Seleccion de Plan

	Scenario Outline: Validacion de cliente ya inscrito
		Given Selecciono el Plan Bci AAdvantage
		When Presiono el boton Quiero este Plan
		And Presiono el boton Comience
		And Ingreso los datos personales "<nombre>" "<apellido_paterno>" "<apellido_materno>" "<rut>" "<nro_serie>" "<telefono>" "<email>"
		And Presiono Continue
		Then Despliega la pantalla de deposito

		Examples:
			| nombre 		| apellido_paterno	| apellido_materno	| rut					| nro_serie		| telefono 	| email								|
			| Fernando	| Montiel						| Riquelme					| 12643142-2	| A369852200	| 987456321	| jpaineq@bcicert.cl	|
			#| Miguel		| Garcia						| Hernandez					| 15070635-1	| A369852200	| 987456321	| jpaineq@bcicert.cl	|
