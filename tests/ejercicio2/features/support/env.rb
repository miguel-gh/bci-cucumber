# -*- coding: UTF-8 -*-
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'capybara-screenshot/cucumber'

browser = ENV['BROWSER']

if browser == 'chrome'
  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end
end

Capybara.default_driver = :selenium

# Capybara Screenshot
Capybara.save_and_open_page_path = './images'
Capybara::Screenshot.autosave_on_failure = true
