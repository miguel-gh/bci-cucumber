Given(/^Soy un usuario interesado en abrir una cuenta corriente$/) do
end

When(/^Ingreso al sitio Viajes Planes$/) do
  visit 'https://www.bci.cl/webPublico/planes.html#/contratoplanes/landing'
  save_screenshot
end

Then(/^Se despliega el landing page de Viajes Planes$/) do
end

Given(/^Ingreso mi edad$/) do
  fill_in 'edad', with: '18'
end

Given(/^Ingreso mi renta$/) do
  find('#renta > a').click
  find('#renta > div > div > input').send_keys('$0 a $399.999')
  find('#ui-select-choices-row-0-0 > div > div').click
end

When(/^Presiono el boton$/) do
    sleep 1
    click_button('Quiero conocer mi plan')
end

Then(/^Aparece el plan universitario$/) do
  expect(page).to have_content("Plan Bci Universitario")
end
