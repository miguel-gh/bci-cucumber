Feature: Desplegar landing page de Viajes Planes
Desplegar a nuevos clientes un landing para abrir cuentas corrientes

Background: Me encuentro en el sitio de Viajes Planes
  Given Soy un usuario interesado en abrir una cuenta corriente
  When Ingreso al sitio Viajes Planes
  Then Se despliega el landing page de Viajes Planes

@smoketest
Scenario: Despliegue de Viajes Planes
  Given Ingreso mi edad
  And Ingreso mi renta
  When Presiono el boton
  Then Aparece el plan universitario

Scenario: No Aparece el plan universitario
  Given Ingreso mi edad
  And Ingreso mi renta
  When Presiono el boton
  Then Aparece el plan universitario
