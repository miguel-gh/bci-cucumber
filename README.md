# Curso Cucumber - Behaviour Driven Development

Curso de Introducción a Cucumber.


## Primeros Pasos

* Tener instalado Ruby
* Instalar **Bundler** `$ gem install bundler`

## Creando un Nuevo Proyecto Cucumber

```
$ cucumber --init
$ ls -R
features

./features:
step_definitions support

./features/step_definitions:

./features/support:
env.rb
```

## Editores de Texto Recomendados

* https://atom.io
* http://www.sublimetext.com

